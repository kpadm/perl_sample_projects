#!/usr/bin/perl

# list_prime_nos.pl
# Get the next prime number in sequence until user input stops
# Karthik Padmanabhan	April 15, 2014	kpadmana@purdue.edu

use strict;
use warnings;
use Getopt::Std;

##--Declarations and initializations go here--##
my %args;
my $num = 0;
my $i = 2;
my $max = 1000000;

sub is_prime{						## subroutine to find the factors of the number
	my ($m) = @_;					## gets the input number and assigns it to a variable
	my $j = 3;						
	my @all_fac;					## array to collect all the factors, will be the return value 
	while ($j <= $m){				
		my $mod1 = $m % $j;			## modulo operator to check for factors
		if ($mod1 == 0){
			push (@all_fac, $j);	## collects all the factors into the array
		}		
		$j += 1;
	}	
	return @all_fac;				## array returned to the main function
}

sub prime_prompt {			## subroutine to get user input for next prime number
	print "Do you want the next prime number?(yes/no)\n";
	my $input = <STDIN>;
	#print "$input\n";
	
	if ($input eq "yes\n"){
		return 1;
		
	} elsif ($input eq "no\n"){
		print "Good bye!\n";
		exit;
		
	} else {
		print "Please type \'yes\' or \'no\'\n";
		goto START;
	}
	
}

getopts('h', \%args);				## using getopts to get the number

if ($args{h}){						## help usage details
	print "Gets the next prime number in sequence until user stops asking for it.\n";
	print "Usage: perl list_prime_nos.pl\n";
	
} else {				
	print "The first prime number is 2.\n";	
	START:		# flag to get the loop started again if current num is prime
	if (&prime_prompt == 1){	# if user types in 'yes'
		MID:	# flag to get loop restarted if current num is not prime
		if ($i <= $max){
			my @prime_fac = &is_prime($i);	#gets the array output from subroutine
			if (scalar(@prime_fac) == 1){	#prime nos. only have one factor, which is the num itself
				print "The next prime number is $i\n";
				
			} else {
				$i += 1;	#number is not prime, start again with the next number
				goto MID;
			}
		}
		$i += 1;	
		goto START;	#go to the beginning of the loop if number is prime
		
	}
		
}
