#!/usr/bin/perl

##	Apr 22, 2014	Karthik Padmanabhan	kpadmana@purdue.edu
##	Program Description: Program to beep system sound after set amount of time, or at a particular time
##	perl_alarm.pl
##	version 1.1

use strict;
use warnings;
use Getopt::Std;

##--Declarations go here--##

my $num;
my $time;
my %args;

##------------------------##

getopts('hsa', %args)
;
print "\07";
