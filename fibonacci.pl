#!/usr/bin/perl

# fibonacci.pl
# When given a number n, gives the output of n fibonacci numbers, or fibonacci numbers upto the value of n
# Karthik Padmanabhan	23 July 2013

use strict;
use warnings;
use Getopt::Std;

my ($fib_val, $fib_num)= 0;
my ($i, $j, $k, $old_i, $old_k) = 0;
my %args;
# -n : number of fibonacci numbers(default)/fibonacci numbers upto n
# -h : help and usage information
# -f : if defined, give fibonacci numbers upto n
getopts('hfn:', \%args);

if ($args{h}){	#checking for help flag, prints out all the usage information
	print "Perl code to print n Fibonacci numbers or fibonacci numbers upto a value of n\n";
	print "Usage: pi_nthdigit.pl -n [positive integer] [options]\n";
	print "where n is the number of fibonacci numbers required\n";
	print "[options] : -f : if defined, give fibonacci numbers upto a value of n instead (default is not defined)\n";	
} elsif (($args{n}) and ($args{f})){	#checking if number is defined, and also the f flag, gives numbers upto n
	$fib_val = $args{n};	#assign the input to $fib_val
	print "The Fibonacci numbers upto $fib_val are: ";
	print "0, 1";
	my $i = 1;
	my $old_i = 0;
	my $new_i = 0;
	while($new_i <= $fib_val){
		$new_i = $i + $old_i;
		print ", $new_i" unless ($new_i > $fib_val);	#prints the number unless it is greater than the input number
		$old_i = $i;
		$i = $new_i; 
		
	}
	
	if($new_i >= $fib_val){
		print "\n";			#prints a new line character if the counter is greater than the input number
	}
} elsif ($args{n}) {		#checks if the number is defined, only goes through if f flag is not defined, so gives n fibonacci numbers
	$fib_num = $args{n};	#assigns the input to $fib_num
	print "The first $fib_num Fibonacci numbers are: ";
	if ($fib_num == 1){
		print "0\n";
		exit;
	} elsif ($fib_num == 2) {
		print "0, 1\n" ;
		exit;
	} else {
		print "0, 1";
	}
	my ($old_k, $new_k) = 0;
	my $j = 3;
	my $k = 1;
	while ($j <= $fib_num){
		
		$new_k = $k + $old_k;
		print ", $new_k";
		$j=$j+1;
		$old_k = $k;
		$k = $new_k;
		
	}
	if ($j > $fib_num){
		print "\n";
	}
} else {
	print "ERROR: Use the flag -h to get usage information\n";	# if nothing is defined, gives error message
}