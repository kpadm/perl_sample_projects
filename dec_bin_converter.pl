#!/usr/bin/perl

##	Apr 18, 2014	Karthik Padmanabhan	kpadmana@purdue.edu
##	Program Description: program to convert decimal to binary and vice-versa 
##	
##	version 1.1

use strict;
use warnings;
use Getopt::Std;

##--Declarations go here--##

my $num;
my $dec;
my $bin;
my %args;

##------------------------##

getopts('hb:d:', \%args);

if ($args{h}){						## help usage details
	print "Converts number between decimal and binary\n";
	print "Usage: perl dec_bin_converter.pl [opts] [number]\n";
	print "Options: -d: Convert decimal number to binary\n";
	print "-b: Convert binary number to decimal\n";
} elsif ($args{b}){
	$bin = $args{b};
	$dec = oct("0b$bin");
	print "$bin in decimal is $dec\n";
} elsif ($args{d}){
	$dec = $args{d};
	$bin = sprintf("%b", $dec);
	print "$dec in binary is $bin\n";
}
