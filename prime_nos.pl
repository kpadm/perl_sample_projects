#!/usr/bin/perl

# prime_new.pl
# Finds if the number is prime or not, also finds the prime factors
# Karthik Padmanabhan	April 14, 2014	kpadmana@purdue.edu

use strict;
use warnings;
use Getopt::Std;

my ($num, $sqrt_num, $mod) = 0;			## declarations and initializations
my $i = 2;
my %args;
my @prime_fac;


getopts('hn:', \%args);				## using getopts to get the number

sub is_prime{						## subroutine to find the factors of the number
	my ($m) = @_;					## gets the input number and assigns it to a variable
	my $j = 2;						
	my @all_fac;					## array to collect all the factors, will be the return value 
	while ($j <= $m){				
		my $mod1 = $m % $j;			## modulo operator to check for factors
		if ($mod1 == 0){
			push (@all_fac, $j);	## collects all the factors into the array
		}		
		$j += 1;
	}	
	return @all_fac;				## array returned to the main function
}

if ($args{h}){						## help usage details
	print "Finds if the given number is prime or not. Also finds all its prime factors.\n";
	print "Usage: perl prime_new.pl -n [positive integer]\n";
	print "where n is the number to be checked\n";
} elsif ($args{n}){				
	$num = $args{n};				## assigns the number to variable $num
	if ($num == 1){		
		print "1\n";				## if the number is 1, prints 1 and exits 
		exit;			
		
	} else {						## runs for all numbers greater than 1
		@prime_fac = &is_prime($num);		## calls the subroutine and gets the array returned
		if (scalar(@prime_fac) > 1){		## if the array size is > 1, it is not a prime number
			print "The number is not prime\nThe list of prime factors:\n";
		} else {							## if array size is exactly 1, which means the number is its own factor, it is a prime no.
			print "The number is prime\n";
			exit;
		}	
	}
}

foreach my $val (@prime_fac){		## this is a loop to find unique prime factors
	my @pr_f = &is_prime($val);		## goes through each of the factors, and checks for prime factors
	if (scalar(@pr_f) == 1){
		print "$pr_f[0]\n";			## only printed when the number is its own factor, which means it is a prime factor
	}
}

