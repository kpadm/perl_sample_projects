#!/usr/bin/perl

##	Apr 16, 2014	Karthik Padmanabhan	kpadmana@purdue.edu
##	Program Description: Calculate number of coins of each denomination, given a purchase and paid amount
##	Currency: USD, Coins: 1c, 5c, 10c, 25c
##	version 1.1

use strict;
use warnings;
use Getopt::Std;

##--Declarations go here--##
my $purc_amt;
my $paid_amt;
my ($q, $d, $n, $p, $diff) = 0;
my %args;

getopts('h', \%args);	

if ($args{h}){						## help usage details
	print "Finds out the change to be given for a transaction\n";
	print "Usage: perl change_calculator.pl\n";
	
} else {
	print "Enter purchase amount:\n";	# gets input from user for purchase 
	$purc_amt = <STDIN>;
	print "Enter paid amount:\n";	# gets input from user for paid amount
	$paid_amt = <STDIN>;
	$diff = $paid_amt - $purc_amt;	# calculates difference between purchase and paid amount
	$diff = $diff * 100;
	$q = int($diff/25);	# number of quarters required
	$diff = $diff % 25;	# money left
	$d = int($diff/10);	# numer of dimes 
	$diff = $diff % 10;	# money left
	$n = int($diff/5);	# number of nickels
	$diff = $diff % 5;	# money left
	$p = $diff;	# number of pennies left
	print "Change: $q quarters, $d dimes, $n nickels, and $p pennies\n."
}

		