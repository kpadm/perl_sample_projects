#!/usr/bin/perl

# pi_nthdigit.pl
# Finds n decimal places of the value of pi, n is given as the input by the user
# Karthik Padmanabhan 	23 July 2013

use strict;
use warnings;
use Getopt::Std;

# value definitions
my ($num, $pi_val, $result);

# code starts here

my %args;
# -n is the number of decimal places
# -h is the help query
getopts('hn:', \%args);

if ($args{h}){		# help documentation, including usage if -h flag is used
	print "Perl code to print the value of pi upto n decimal places\n";
	print "Usage: pi_nthdigit.pl -n \n";
	print "where n is the number of decimal places, can take a value upto 51\n";
} elsif ($args{n}){		#only enters the loop if n is defined
	$num = $args{n};
	if (($num > 51) or ($num !~ /^[0-9]+$/) or ($num < 1)){		#checks for max value of n and if n is integer > 0
		print "ERROR: Please enter a number greater than 0 and less than or equal to 51\n";
		exit;
	}
	$pi_val = (22/7);
	$result = sprintf("%.${num}f", $pi_val); #defines the number of decimal places according to user input
	print "The value of pi upto $num decimal places is $result\n";	#prints out the result
} else{
	print "ERROR: Use the flag -h to get usage information\n";	# if nothing is defined, gives error message 
}