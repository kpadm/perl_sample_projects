#!/usr/bin/perl
use lib '/home/kpadmana/bin_local/lib/perl5/site_perl/5.8.8/';


##	Apr 18, 2014	Karthik Padmanabhan	kpadmana@purdue.edu
##	Program Description: Calculates distance between two given cities using coordinates
##	cities_distance.pl
##	version 1.1

use strict;
use warnings;
use Geo::Coder::Google;
use Data::Dumper;
use Geo::Coder::Google;

##--Declarations go here--##
my $city1;
my $city2;
my $loc1;
my $loc2;
my ($lat1, $lat2, $lon1, $lon2);

##------------------------##

sub geoLocator{
	my ($city) = @_;
	#print "@city\n";
	my $geocoder = Geo::Coder::Google->new(apiver => 3);
	my $location = $geocoder->geocode(location => "$city");
	return $location;
}

print "Enter first location as [City, State, Country]:\n";
$city1 = <STDIN>;
$loc1 = &geoLocator($city1);
print "Enter second location as [City, State, Country]:\n";
$city2 = <STDIN>;
$loc2 = &geoLocator($city2);

$lat1 = $$loc1{'geometry'}{'location'}{'lat'};
$lon1 = $$loc1{'geometry'}{'location'}{'lng'};
$lat2 = $$loc2{'geometry'}{'location'}{'lat'};
$lon2 = $$loc2{'geometry'}{'location'}{'lng'};

#print "$lat1\t$lon1\t$lat2\t$lon2\n";
if ($lat1 > 0) && ($lat2 > 0){
	
}



