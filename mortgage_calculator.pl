#!/usr/bin/perl

##	Apr 17, 2014	Karthik Padmanabhan	kpadmana@purdue.edu
##	Calculates monthly payments of a fixed term mortgage over given Nth terms at a given interest rate
##	Also calculates time take to pay the user back the loan
##	version 1.1

use strict;
use warnings;
use Getopt::Std;

##--Declarations go here--##
my $payment;
my $principal;
my $i_rate;
my $n_months;
my $flag;
my %args;
##------------------------##

 sub log10 {	# subroutine to find log base 10 
    my $n = shift;
 	return log($n)/log(10);
 }

sub getInput{	# subroutine to calculate mortgage payments/time
	my ($val) = @_;
	print "Enter mortgage amount borrowed:\n";
	$principal = <STDIN>;

	print "Enter annual interest rate:\n";
	$i_rate = <STDIN>;
	$i_rate = ($i_rate/100)/12; # interest rate needs to be converted into a monthly rate
	
	
	if ($val eq "mor"){	# runs if value passed from main is "mor""
		print "Enter duration of mortgage (years):\n";
		$n_months = <STDIN>;
		$n_months = $n_months * 12;	# converts input into no. of months
		my $j = 1 + $i_rate;
		
		my $i = ($j) ** $n_months;
		
		$payment = ($principal * (($i_rate*$i))/($i - 1));	# formula for calculating mortgage
		print "The monthly mortgage payment is $payment.\n";
		exit; # job done, exit
	} elsif ($val eq "time"){ 	# if value passed from main is "time"
		print "Enter requested monthly payment:\n";
		$payment = <STDIN>;
		my $x = (1/(1-($principal*$i_rate/$payment))); #forumula for mortgage duration
		$x = &log10($x);
		my $y = 1 + $i_rate;
		$y = &log10($y);
		$n_months = $x / $y;
		print "The number of months required is $n_months.\n";
		exit; # job done, exit
	}
}

getopts('hmt', \%args);

if ($args{h}){						## help usage details
	print "Finds out monthly payments/time required to pay off of a fixed term mortgage.\n";
	print "Usage: perl mortgage_calculator.pl [opts]\n";
	print "Options: -m: Calculate monthly payments given duration and interest rate.\n";
	print "-t: Calculate time taken to pay off, given payments and interest rate.\n";
} elsif ($args{m}){
	$flag = "mor";
	&getInput($flag); # passes 'mor' to subroutine if user wants mortgage calculation
} elsif ($args{t}){
	$flag = "time";
	&getInput($flag); # passes 'time' to subroutine if user wants time calculation
}